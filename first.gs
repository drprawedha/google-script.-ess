/**
 * Membuat sebuah Google Doc dan mengirimnya ke email user aktif lengkap dengan tautannya.
*/
function createAndSendDocument() {
  // Buat berkas dengan nama Hello,World
  var doc = DocumentApp.create('Hello, world!');

  // Akses pada konten lalu menambahkan paragraf dibawah..
  doc.getBody().appendParagraph('This document was created by Google Apps Script.');

  // mendapatkan url berkas
  var url = doc.getUrl();

  // Mendapatkan email aktif dari pengguna yang mengakses.
  var email = Session.getActiveUser().getEmail();

  // Memberi nama subject mail dengan nama dokumen yang dikirim.
  var subject = doc.getName();

  // Menyisipkan kata kata dalam surel beserta dengan tautan yang menuju dokumen
  var body = 'Tautan anda berada di: ' + url;

  // Perintah eksekusi.
  GmailApp.sendEmail(email, subject, body);
}
